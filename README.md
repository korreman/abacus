ABACUS
======
A math and base conversion game.

Brainstorming
-------------
Game needs to support practice of:
* Conversion between all bases
... Needs features like only needing to convert certain significant digits
* Practicing addition, subtraction, multiplication and division in all bases

### Planned features:
Terminal:
* Correct/Incorrect (See Freaking Math)
* Multiple answers
* Type in the answer
* Increasing difficulty or Limited set of questions.

GUI:
* Timed/free play
* Eventual assisted mode (Like Binary android game)

### Structure:
* A problem generator
* A problem type (struct/class of some sort)
* UI interface, for later OpenGL expansion

### Game structure:
1. Manager acquires settings through IO\_Interface
in a loop:
2. Manager creates Problem.
3. Sends Problem to User Interface.
4. Store problem in list.
5. Repeat.
