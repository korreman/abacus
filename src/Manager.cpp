#include <Manager.h>
#include <Generator.h>
#include <Validation.h>
#include <vector>

bool doQuit(std::string answer)
{
    return answer == "q";
}

void Manager::run(UserInterface& ui)
{
    GameMode mode {ui.getMode()};
    Generator generator {mode};

    std::vector<bool> history;
    bool stop {false};
    while (!stop)
    {
        Problem p = generator.genProblem();
        std::string answer = ui.presentProblem(p);

        stop = doQuit(answer);
        if (!stop)
        {
            bool isCorrect {validate(mode, answer, p.solution)};
            ui.presentAnswer(isCorrect);
            history.push_back(isCorrect);
        }
    }

    ui.presentResults(history);
}
