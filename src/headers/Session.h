#ifndef SESSION_H
#define SESSION_H

#include <SDL2/SDL.h>

class Session
{
private:
    SDL_Window* window {NULL};
    SDL_Surface* surface {NULL};

public:
    SDL_Window* getWindow() { return window; }
    void init();
    void close();
};

#endif
