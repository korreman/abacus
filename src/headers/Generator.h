#ifndef PROBLEMGENERATOR_H
#define PROBLEMGENERATOR_H

#include <Config.h>
#include <Problem.h>
#include <Conversion.h>

// Generates problems for the player to solve, contains state information on
// current configuration
class Generator
{
private:
    GameMode m;

    number maxVal {255};
    number extendVal {0};

    bool isSigned {false};
    bool qBasePow2 {false};
    bool aBasePow2 {false};

    // slightly changes the value within a boundary given in @m
    number changeVal(const number& val);

    // generates a problem question and sets @aVal to the correct answer
    std::string genQuestion(number& aVal);

    // generates a set of options based on @aVal, uses @changeVal
    std::vector<std::string> genOptions(const number& aVal);

public:
    // initializes the generator with configuration @mode
    Generator(GameMode mode);

    // changes the configuration to @mode
    void setMode(GameMode mode);

    // generates a random problem based on current configuration
    Problem genProblem();
};

#endif
