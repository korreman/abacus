#ifndef TEXTUI_H
#define TEXTUI_H

#include <UserInterface.h>

// Text user interface
class TextUI : public UserInterface
{
private:
    int aBase {10};
    int qBase {10};

public:
    // Retrieves a game configuration
    GameMode getMode();

    // Presents @problem and retrieves an answer
    std::string presentProblem(const Problem &problem);

    // Presents whether the @answer was correct or not
    void presentAnswer(const bool &answer);

    // Presents the results in @history after a game has been played
    void presentResults(std::vector<bool> history);
};

#endif
