#ifndef CONVERSION_H
#define CONVERSION_H

#include <string>

typedef unsigned long number;

// converts @val to a string in base @base, as though it was signed
std::string number2str(number val, int base);

#endif
