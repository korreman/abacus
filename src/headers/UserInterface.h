#ifndef USERINTERFACE_H
#define USERINTERFACE_H

#include <string>
#include <vector>
#include <Problem.h>
#include <Config.h>

// Interface for user interactions
class UserInterface
{
public:
    // Dummy constructor and destructor
    virtual ~UserInterface() {}

    // Retrieves a game configuration
    virtual GameMode getMode() = 0;

    // Presents @problem and retrieves an answer
    virtual std::string presentProblem(const Problem &problem) = 0;

    // Presents whether the @answer was correct or not
    virtual void presentAnswer(const bool &answer) = 0;

    // Presents the results in @history after a game has been played
    virtual void presentResults(std::vector<bool> history) = 0;
};

#endif
