#ifndef BASICIO_H
#define BASICIO_H

#include <vector>
#include <string>

// Prints @text and prompts for a yes or no
bool getBool(std::string text);

// Prints @text and retrieves a long between @mix and @max
long getLong(std::string text, long min, long max);

// Prints @text and @options and retrieves one of the options
long getOption(std::string text, std::vector<std::string> options);

#endif

