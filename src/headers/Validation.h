#ifndef VALIDATION_H
#define VALIDATION_H

#include <string>
#include <Config.h>

// Confirms whether @answer matches @solution, taking game config into account
bool validate(const GameMode& mode,
              std::string answer, const std::string& solution);
#endif
