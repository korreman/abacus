#ifndef MANAGER_H
#define MANAGER_H

#include <UserInterface.h>

// Manages getting settings, generating problems, and passing them to the UI
class Manager
{
public:
    // Runs the game
    void run(UserInterface& ui);
};

#endif
