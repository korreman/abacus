#ifndef PRNG_H
#define PRNG_H

#include <random>

namespace PRNG
{
    // Initializes the generator with a seed
    void init();
    // Returns a random integer between @min and @max
    long rndInt(long min, long max);
    // Returns a random floating point between @min and @max
    double rndFloat(double min, double max);
}

#endif

