#ifndef CONFIG_H
#define CONFIG_H

// Types for specifying problem mode
enum MODEenum {MODE_CONVERSION, MODE_ADDITION, MODE_SUBTRACTION,
               MODE_MULTIPLICATION};
enum ANSWERenum {ANSWER_DIRECT, ANSWER_CONFIRM, ANSWER_MULTIPLE};

// Contains information about the current game configuration
struct GameMode
{
    // Modes and bases
    MODEenum qMode {MODE_CONVERSION};
    ANSWERenum aMode {ANSWER_DIRECT};
    int qBase {10};
    int aBase {10};

    // Random number size and signature
    int digits {2};
    bool isSigned {false};

    // Incorrect answer variance and option count
    double variance {0.1};
    int count {4};
};

#endif
