#ifndef PROBLEM_H
#define PROBLEM_H

#include <string>
#include <vector>

// Problem structure
struct Problem
{
    Problem(std::string q, std::string s, std::vector<std::string> o)
        : question(q), solution(s), options(o) {}

    // The problem text
    std::string question {"DEFAULT QUESTION STRING"};

    // The solution text
    std::string solution {"DEFAULT SOLUTION STRING"};

    // An optional list of options to choose from
    std::vector<std::string> options;
};

#endif
