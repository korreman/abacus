#include <BasicIO.h>
#include <iostream>
#include <limits>
using std::string;

bool getBool(string text)
{
    string input;
    while (input != "y" && input != "n")
    {
        std::cout << text << " [y/n] ";
        std::cin >> input;
    }
    return input == "y";
}

long getLong(string text, long min, long max)
{
    long input {min - 1};
    while (input < min || input > max)
    {
        std::cout << text;
        if (!(std::cin >> input))
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            input = min - 1;
        }
    }
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    return input;
}

long getOption(string text, std::vector<string> options)
{
    int i {0};
    std::cout << text << '\n';
    for (auto option : options)
    {
        std::cout << "  " << i++ << " " << option << '\n';
    }
    long retval = getLong("Select: ", 0, options.size() - 1);
    std::cout << '\n';
    return retval;
}
