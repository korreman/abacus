#include <algorithm>
#include <math.h>
#include <Generator.h>
#include <Problem.h>
#include <PRNG.h>
using std::string;

#include <iostream>
bool isPowerOf2(const number &n)
{
    return n && !(n & (n-1));
}

Generator::Generator(GameMode mode)
{
    setMode(mode);
}

void Generator::setMode(GameMode mode)
{
    m = mode;
    qBasePow2 = isPowerOf2(m.qBase);
    aBasePow2 = isPowerOf2(m.aBase);
    maxVal = pow(mode.isSigned ? 2 : m.qBase, mode.digits) - 1;

    // used for sign extension
    extendVal = 1u << (m.digits - 1);
}

string Generator::genQuestion(number& aVal)
{
    if (m.qMode == MODE_CONVERSION)
    {
        number val = PRNG::rndInt(1, maxVal);
        number qVal = aVal = val;
        if (m.isSigned)
        {   // sign extension. result used for values that aren't a power of 2
            number signedVal = (aVal ^ extendVal) - extendVal;
            aVal = aBasePow2 ? val : signedVal;
            qVal = qBasePow2 ? val : signedVal;
        }
        return number2str(qVal, m.qBase);
    }
    else
    {
        const static std::vector<string> operators {" + ", " - ", " * ", " / "};
        number val1 = PRNG::rndInt(1, maxVal);
        number val2 = PRNG::rndInt(1, maxVal);
        switch(m.qMode)
        {
            case MODE_ADDITION:       aVal = val1 + val2; break;
            case MODE_SUBTRACTION:    aVal = val1 - val2; break;
            case MODE_MULTIPLICATION: aVal = val1 * val2; break;
            default: throw "Unhandled question mode";
        }

        return number2str(val1, m.qBase) + operators.at(m.qMode - 1)
             + number2str(val2, m.qBase);
    }
}

number Generator::changeVal(const number& val)
{
    return val * PRNG::rndFloat(1.0 - m.variance, 1.0 + m.variance);
}

std::vector<string> Generator::genOptions(const number& aVal)
{
    // Generate vector the answer, change all, reset one of them to the solution
    std::vector<number> options(m.count, aVal);
    for (number& val : options) { val = changeVal(val); }
    options.at(PRNG::rndInt(0, m.count - 1)) = aVal;

    // Return vector of converted strings
    std::vector<string> retval;
    for (number val : options)
    {
        retval.push_back(number2str(val, m.aBase));
    }
    return retval;
}

Problem Generator::genProblem()
{
    number aVal;

    string question = genQuestion(aVal);
    string solution;
    std::vector<string> options;

    if (m.aMode == ANSWER_DIRECT )
    {
        solution = number2str(aVal, m.aBase);
    }
    else if (m.aMode == ANSWER_MULTIPLE)
    {
        options = genOptions(aVal);
        solution = number2str(aVal, m.aBase);
    }
    else if (m.aMode == ANSWER_CONFIRM)
    {
        // 50% chance of changing the value
        bool correct = PRNG::rndInt(0, 1);
        if (!correct) { aVal = changeVal(aVal); }
        solution = correct ? "yes" : "no";

        question += " = " + number2str(aVal, m.aBase);
        options = {"no", "yes"};
    }

    return Problem {question, solution, options};
}
