#include <PRNG.h>
#include <random>

namespace PRNG
{
    std::ranlux48_base generator;
    bool seeded {false};

    void seed()
    {
        std::random_device rd;
        generator.seed(rd());
        seeded = true;
    }

    long rndInt(long min, long max)
    {
        if (!seeded) { seed(); }
        std::uniform_int_distribution<long> dist(min, max);
        return dist(generator);
    }

    double rndFloat(double min, double max)
    {
        if (!seeded) { seed(); }
        std::uniform_real_distribution<double> dist(min, max);
        return dist(generator);
    }

}
