#include <Conversion.h>
using std::string;

string number2str(number val, int base)
{
    static string symbols {"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"};
    string sign;
    if (val >> (sizeof(number) * 8 - 1)) // check the negative bit
    {
        val = -val;
        sign = "-";
    }

    string retval;
    while(val)
    {
        retval = symbols.at(val % base) + retval;
        val /= base;
    }

    return sign + retval;
}
