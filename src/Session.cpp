#include <Session.h>
#include <iostream>
#include <exception>
#include <SDL2/SDL_ttf.h>

void error(const std::string& message)
{
    throw message + SDL_GetError();
    SDL_ClearError();
}

// Initializes modules and creates a window
void Session::init()
{
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        error("SDL_Init failed: ");
    }

    if (TTF_Init() != 0)
    {
        throw "TTF_Init failed: " + std::string{TTF_GetError()};
    }

    // Create window
    window = SDL_CreateWindow("Abacus",
                              SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              800, 450, SDL_WINDOW_SHOWN);
    if (window == NULL)
    {
        error("Window creation failed: ");
    }

    surface = SDL_GetWindowSurface(window);
    if (surface == NULL)
    {
        error("Surface creation failed: ");
    }
}

void Session::close()
{
    // FIXME: Error handling
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
}
