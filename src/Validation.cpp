#include <Validation.h>
#include <algorithm>
using std::string;

std::string processAnswer(string answer, int base)
{
    // make uppercase, remove spaces
    std::transform(answer.begin(), answer.end(), answer.begin(), ::toupper);
    answer.erase(std::remove(answer.begin(), answer.end(), ' '), answer.end());

    // choose eventual prefix character
    char prefix;
    switch(base)
    {
        case 2:  prefix = 'B'; break;
        case 16: prefix = 'X'; break;
        default: prefix = ' '; break;
    }

    // remove leading zeroes
    while (answer.size() > 0 && answer.at(0) == '0')
    {
        answer = answer.erase(0, 1);
    }

    // remove leading prefix
    if (answer.size() > 0 && answer.at(0) == prefix)
    {
        answer = answer.erase(0, 1);
    }
    return answer;
}

bool validate(const GameMode& mode, string answer, const string& solution)
{
    // sanitize the answer if the answer is typed in by the user
    if (mode.aMode == ANSWER_DIRECT)
    {
        answer = processAnswer(answer, mode.aBase);
    }
    return answer == solution;
}
