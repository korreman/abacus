#include <iostream>
#include <TextUI.h>
#include <BasicIO.h>
using std::vector;
using std::string;

GameMode TextUI::getMode()
{
    static vector<string> qModeStrs{"Base conversion", "Addition",
        "Subtraction", "Multiplication", "Division"};
    static vector<string> aModeStrs{"Direct answer", "Confirm answer",
        "Multiple choice"};

    GameMode mode;
    mode.qMode = (MODEenum)getOption("Game mode", qModeStrs);
    mode.aMode = (ANSWERenum)getOption("Answering mode", aModeStrs);
    mode.qBase = mode.aBase = getLong("Choose a base: ", 2, 32);
    if (mode.qMode == MODE_CONVERSION)
    {
        mode.aBase = getLong("Choose second base: ", 2, 32);
        mode.isSigned = getBool("Signed mode?");
    }

    string rangeType = mode.isSigned ? " bits " : " digits ";
    mode.digits = getLong("How many" + rangeType + "to generate? ", 1, 64);

    qBase = mode.qBase;
    aBase = mode.aBase;

    return mode;
}

string TextUI::presentProblem(const Problem &problem)
{
    if (problem.options.size() > 0)
    {
        long answer = getOption(problem.question, problem.options);
        return problem.options.at(answer);
    }

    static vector<string> bases{"ERR", "ERR", "BIN", "TRN", "QUA", "PEN",
        "SEN", "SEP", "OCT", "NON", "DEC", "UND", "DOZ", "TRI", "TET", "PND",
        "HEX"};
    std::cout << bases.at(qBase) << ": "
              << problem.question << '\n'
              << bases.at(aBase) << ": ";

    string answer;
    std::getline(std::cin, answer);

    return answer;
}

void TextUI::presentAnswer(const bool &answer)
{
    std::cout << (answer ? "Correct!" : "Incorrect!") << "\n\n";
}

void TextUI::presentResults(vector<bool> history)
{
    long sum {0};
    for (auto isCorrect : history)
    {
        sum += isCorrect ? 1 : 0;
    }
    double acc = ((double)sum) / (double(history.size()));
    std::cout.precision(4);
    std::cout << "\nAccuracy: " << acc * 100 << "%\n";
}
