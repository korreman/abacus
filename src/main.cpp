#include <Manager.h>
#include <TextUI.h>

#include <iostream>
#include <Session.h>
#include <SDL_ttf.h>
#include <SDL_rwops.h>
int main()
{
    Session session;
    session.init();
    SDL_Window* window = session.getWindow();
    SDL_Renderer* renderer = SDL_CreateRenderer(window, -1, 0);

    SDL_RWops* fontfile = SDL_RWFromFile("FreeSans.ttf", "r");
    TTF_Font* font = TTF_OpenFontIndexRW(fontfile, 1, 16, 0);
    std::cout << TTF_GetError() << '\n';

    SDL_SetRenderDrawColor(renderer, 0xFF, 0x0F, 0xFF, 0xFF);
    SDL_RenderClear(renderer);
    SDL_RenderPresent(renderer);

    SDL_Color color {0, 0, 0, 1};
    SDL_Surface* hello = TTF_RenderText_Blended(font, "Hello, world!", color);
    //SDL_Texture* helloTex = SDL_CreateTextureFromSurface(renderer, hello);

    //SDL_RenderCopy(renderer, helloTex, NULL, NULL);

    SDL_Delay(3000);

    SDL_FreeSurface(hello);
    //SDL_DestroyTexture(helloTex);
    //TTF_CloseFont(font);
    SDL_DestroyRenderer(renderer);
    session.close();

    //TextUI ui;
    //Manager manager;
    //manager.run(ui);

    return 0;
}
